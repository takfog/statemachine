﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace StateMachine
{
    public delegate bool TransitionManager<T>(string state, T value);

    public class MachineBuilder<T>
    {
        private const string SINK = "sink";
        private Dictionary<string, BuildingState> states = new Dictionary<string, BuildingState>();
        private Dictionary<string, TransitionCondition<T>> conditions = new Dictionary<string, TransitionCondition<T>>();
        private Dictionary<string, TransitionEventHandler<T>> events = new Dictionary<string, TransitionEventHandler<T>>();
        private IList<string> transOrder;
        public TransitionManager<T> TransitionManager { get; set; }

        public MachineBuilder()
        {
            TransitionManager = null;
        }

        public MachineBuilder(string data) : this(new StringReader(data)) { }

        public MachineBuilder(TextReader reader) : this()
        { 
            int id = 0;
            string[] headers = null;
            int any = -1, enter = -1;
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (headers == null)
                {
                    headers = line.Split(',');
                    transOrder = new List<string>(headers.Length);
                    for (int i = 1; i < headers.Length; i++)
                    {
                        switch (headers[i].ToLower())
                        {
                            case "any":
                                any = i;
                                break;
                            case "enter":
                                enter = i;
                                break;
                            default:
                                transOrder.Add(headers[i]);
                                break;
                        }
                    }
                    continue;
                }
                string[] parts = line.Split(',');
                BuildingState st = new BuildingState(this, id++, parts[0]);
                states[st.name] = st;
                for (int i = 1; i < parts.Length; i++)
                {
                    if (parts[i] == "")
                        continue;
                    if (i == enter)
                        st.onEnter = parts[i];
                    else
                    {
                        int actSep = parts[i].IndexOf("->");
                        string next, act = null;
                        if (actSep < 0)
                            next = parts[i];
                        else
                        {
                            next = actSep == 0 ? st.name
                                    : parts[i].Substring(0, actSep);
                            act = parts[i].Substring(actSep + 2);
                        }
                        if (next == SINK && !states.ContainsKey(SINK))
                            states[SINK] = new BuildingState(this, id++, SINK);

                        if (i == any)
                        {
                            st.any = next;
                            st.anyAct = act;
                        }
                        else
                        {
                            st.trans[headers[i]] = next;
                            if (act != null)
                                st.transAct[headers[i]] = act;
                        }
                    }
                }
            }
        }

        public MachineBuilder<T> AddEvent(string name, TransitionEventHandler<T> ev)
        {
            events[name] = ev;
            return this;
        }

        public MachineBuilder<T> AddCondition(string name, TransitionCondition<T> cond)
        {
            conditions[name] = cond;
            return this;
        }

        public MachineBuilder<T> SetTransitionManager(TransitionManager<T> transMan)
        {
            TransitionManager = transMan;
            return this;
        }

        public int GetId(string name)
        {
            if (states.ContainsKey(name))
                return states[name].id;
            else
                return -1;
        }

        public ICollection<string> States
        {
            get
            {
                return states.Keys;
            }
        }

        public ICollection<string> GetNextStates(string state)
        {
            return states[state].trans.Values;
        }

        public StateMachine<T> Build()
        {
            return Build(null);
        }

        public StateMachine<T> Build(string start)
        {
            ICollection<State<T>> finalStates = new List<State<T>>(states.Count);
            TransitionManager<T> transManager = TransitionManager != null ? TransitionManager : (s,v) => v.Equals(s);
            Dictionary<string, TransitionCondition<T>> managedCond = new Dictionary<string, TransitionCondition<T>>();
            foreach (BuildingState st in states.Values)
            {
                State<T> newSt = new State<T>(st.id);
                finalStates.Add(newSt);
                if (start == null && st.name.StartsWith("*"))
                    start = st.name;

                if (st.onEnter != null)
                {
                    if (st.onEnter.Contains("+"))
                    {
                        foreach (string ev in st.onEnter.Split('+'))
                        {
                            newSt.Entering += this.events[ev];
                        }
                    }
                    else
                    {
                        newSt.Entering += this.events[st.onEnter];
                    }
                }
                foreach (string transName in transOrder)
                {
                    if (st.trans.ContainsKey(transName))
                    {
                        TransitionCondition<T> cond = null;
                        if (conditions.ContainsKey(transName))
                            cond = conditions[transName];
                        else if (managedCond.ContainsKey(transName))
                            cond = managedCond[transName];
                        else
                        {
                            cond = v => transManager(transName, v);
                            managedCond[transName] = cond;
                        }

                        Transition<T> trans = new Transition<T>(cond, states[st.trans[transName]].id);
                        if (st.transAct.ContainsKey(transName))
                            RegisterEvents(trans, st.transAct[transName]);
                        newSt.AddTransition(trans);
                    }
                }
                if (st.any != null)
                    newSt.AddTransition(st.AnyTrans);
            }
            return new StateMachine<T>(start == null ? 0 : states[start].id, finalStates);
        }

        private void RegisterEvents(Transition<T> trans, string events)
        {
            RegisterEvents(trans, true, events);
        }

        private void RegisterEvents(Transition<T> trans, bool isTrans, string events) 
        {
            if (events == null) return;
            if (events.Contains("+"))
            {
                foreach (string ev in events.Split('+'))
                {
                    trans.Transiting += this.events[ev];
                }
            }
            else
            {
                trans.Transiting += this.events[events];
            }
        }

        private class BuildingState
        {
            private MachineBuilder<T> parent;
            public int id;
            public string name, onEnter;
            public string any, anyAct;
            public Dictionary<string, string> trans = new Dictionary<string, string>();
            public Dictionary<string, string> transAct = new Dictionary<string, string>();

            public BuildingState(MachineBuilder<T> parent, int id, string name)
            {
                this.parent = parent;
                this.name = name;
                this.id = id;
            }

            public ElseTransition<T> AnyTrans
            {
                get
                {
                    return BuildTrans(any, anyAct);
                }
            }

            private ElseTransition<T> BuildTrans(string state, string act)
            {
                if (state == null)
                    return null;
                if (act == null)
                    return new ElseTransition<T>(parent.states[state].id);
                ElseTransition<T> trans = new ElseTransition<T>(parent.states[state].id);
                parent.RegisterEvents(trans, act);
                return trans;
            }
        }
    }
}

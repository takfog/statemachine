﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StateMachine
{
    class State<T>
    {
        public TransitionEventHandler<T> Entering;
        private IList<Transition<T>> trans = new List<Transition<T>>();

        public int Id { get; private set; }

        public State(int id)
        {
            this.Id = id;
        }

        public State<T> AddTransition(Transition<T> trans)
        {
            this.trans.Add(trans);
            return this;
        }

        public bool isFinal()
        {
            return trans.Count == 0;
        }

        public int Transit(T value)
        {
            foreach (Transition<T> t in trans)
            {
                if (t.CanTransit(Id, value))
                {
                    return t.Transit(Id, value);
                }
            }
            return -1;
        }

        public void Enter(int from, T value)
        {
            if (Entering != null)
                Entering(value);
        }
    }
}

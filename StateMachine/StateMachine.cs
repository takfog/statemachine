﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StateMachine
{
    public class StateMachine<T>
    {
        private State<T> current;
        private State<T>[] states;
        private int start;

        internal StateMachine(int start, ICollection<State<T>> states)
        {
            int maxId = 0;
            foreach (State<T> s in states)
            {
                if (s.Id > maxId)
                    maxId = s.Id;
            }
            this.states = new State<T>[maxId + 1];
            foreach (State<T> s in states)
            {
                this.states[s.Id] = s;
            }
            this.start = start;
            Reset();
        }

        public int State
        {
            get
            {
                return current.Id;
            }
        }

        public bool Next(T value)
        {
            int from = current.Id;
            int dest = current.Transit(value);
            if (dest >= 0)
            {
                current = states[dest];
                current.Enter(from, value);
            }
            return !current.isFinal();
        }

        public void Reset()
        {
            current = states[start];
        }
    }
}

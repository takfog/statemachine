﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StateMachine
{
    class ElseTransition<T> : Transition<T>
    {

        public ElseTransition(int dest) : base(null, dest) { }

        public override bool CanTransit(int from, T value)
        {
            return true;
        }

    }
}

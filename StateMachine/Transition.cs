﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StateMachine
{
    public delegate void TransitionEventHandler<T>(T value);
    public delegate bool TransitionCondition<T>(T value);

    class Transition<T>
    {
        private int dest;
        private TransitionCondition<T> test;
        public event TransitionEventHandler<T> Transiting;

        public Transition(TransitionCondition<T> test, int dest) {
            this.test = test;
            this.dest = dest;
        }

        public virtual bool CanTransit(int from, T value)
        {
            return test(value);
        }

        public int Transit(int from, T value)
        {
            if (Transiting != null)
                Transiting(value);
            return dest;
        }
    }
}
